﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Board : MonoBehaviour
{
    private Boat boat;
    private Vector2 firstTouchPosition;
    private Vector2 finalTouchPosition;
    private int moveX = 1;

    private void Start()
    {
        boat = FindObjectOfType<Boat>();
    }

    private void OnMouseDown()
    {
        firstTouchPosition = Camera.main.WorldToScreenPoint(Input.mousePosition);
    }

    private void OnMouseUp()
    {
        finalTouchPosition = Camera.main.WorldToScreenPoint(Input.mousePosition);

        if (finalTouchPosition.x > firstTouchPosition.x)
            boat.Move(moveX);
        else if (finalTouchPosition.x < firstTouchPosition.x)
            boat.Move(-moveX);
    }
}
