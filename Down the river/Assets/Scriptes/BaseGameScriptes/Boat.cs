﻿using System.Collections;
using UnityEngine;

public class Boat : MonoBehaviour
{
    public int speed;
    public BoatState boatState;
    private Rigidbody2D rigidbody;
    private float maxX = 2.0f;
    private float minX = -2.0f;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boatState = BoatState.Move;
    }

    private void Update()
    {
        StayingInBoard();
    }

    public void Move(int moveX)
    {
        if (boatState == BoatState.Move)
        {
            Vector2 movement = new Vector2(moveX, 0.0f);
            rigidbody.velocity = movement * speed;
            boatState = BoatState.Wait;
            StartCoroutine(StopMoveCo());
        }
    }

    private IEnumerator StopMoveCo()
    {
        yield return new WaitForSeconds(.5f);
        Vector2 movement = new Vector2(0.0f, 0.0f);
        rigidbody.velocity = movement;
        boatState = BoatState.Move;
    }

    private void StayingInBoard()
    {
        float tempPosition = Mathf.Clamp(gameObject.transform.position.x, minX, maxX);
        gameObject.transform.position = new Vector3(tempPosition, gameObject.transform.position.y);
    }
}

public enum BoatState
{
    Move,
    Wait
}
