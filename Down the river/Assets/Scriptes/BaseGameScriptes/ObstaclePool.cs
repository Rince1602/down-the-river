﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePool : MonoBehaviour
{
    public Obstacle[] obstacles;
    private List<Obstacle> obstaclesPool;
    public Transform obstaclesSpawnPoint;
    public int obstaclesCount;
    private int obstaclesUntilSpeedUp = 0;
    public int ObstaclesUntilSppedUp
    {
        get { return obstaclesUntilSpeedUp; }
    }
    public float cooldownTime;
    private bool isCooldown = false;    

    private void Start()
    {
        obstaclesPool = new List<Obstacle>();
        for (int i = 0; i < obstaclesCount; i++)
        {            
            var obstacleTemp = Instantiate(obstacles[Random.Range(0, obstacles.Length)], obstaclesSpawnPoint);
            obstaclesPool.Add(obstacleTemp);
            obstacleTemp.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (!isCooldown)
        {
            GetObstacleFromPool();
            isCooldown = true;
        }
    }

    private Obstacle GetObstacleFromPool()
    {       
        if (obstaclesCount > 0)
        {
            var obstacleTemp = obstaclesPool[Random.Range(0, obstaclesCount - 1)];
            obstaclesPool.Remove(obstacleTemp);
            obstacleTemp.gameObject.SetActive(true);
            obstacleTemp.transform.parent = null;
            int randomLane = Random.Range(1, 4);

            if (randomLane == 1)
            {
                obstacleTemp.transform.position = new Vector2(-2.0f, obstaclesSpawnPoint.transform.position.y);
            }
            if (randomLane == 2)
            {
                obstacleTemp.transform.position = obstaclesSpawnPoint.transform.position;
            }
            if (randomLane == 3)
            {
                obstacleTemp.transform.position = new Vector2(2.0f, obstaclesSpawnPoint.transform.position.y);
            }

            StartCoroutine(PoolCooldownCo());
            obstaclesUntilSpeedUp += 1;
            return obstacleTemp;            
        }

        return Instantiate(obstacles[0], obstaclesSpawnPoint.position, Quaternion.identity);
    }

    public void ReturnObstacleToPool(Obstacle obstacleTemp)
    {
        if (!obstaclesPool.Contains(obstacleTemp))
        {
            obstaclesPool.Add(obstacleTemp);
        }

        obstacleTemp.transform.parent = obstaclesSpawnPoint;
        obstacleTemp.transform.position = obstaclesSpawnPoint.transform.position;
        obstacleTemp.gameObject.SetActive(false);
    }

    private IEnumerator PoolCooldownCo()
    {
        yield return new WaitForSeconds(cooldownTime);
        isCooldown = false;
        if (obstaclesUntilSpeedUp % 10 == 0)
        {
            cooldownTime -= 0.1f;
        }
        else if (cooldownTime <= 1.5f)
        {
            cooldownTime = 1.5f;
        }
    }
}
