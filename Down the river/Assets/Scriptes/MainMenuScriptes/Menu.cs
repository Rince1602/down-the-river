﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Text highScore;

    private void Start()
    {
        if (PlayerPrefs.HasKey("highScore"))
        {
            highScore.text = "" + PlayerPrefs.GetInt("highScore");
        }
        else
        {
            highScore.text = "0";
            PlayerPrefs.SetInt("highScore", 0);
        }
    }

    public void StartButton()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
