﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float speed;
    private ObstaclePool obstaclePool;
    [SerializeField] private Rigidbody2D rigidbody;

    private void Start()
    {
        obstaclePool = FindObjectOfType<ObstaclePool>();
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {

        rigidbody.velocity = Vector2.down * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Time.timeScale = 0;
            UIController.Instance.SetHighScore();
            UIController.Instance.losePanel.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {        
        obstaclePool.ReturnObstacleToPool(this);
    }
}
