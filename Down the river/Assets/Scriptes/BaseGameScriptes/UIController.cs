﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public static UIController Instance { get; set; }

    [Header("Pause Stuff")]
    private bool isPaused = false;
    private bool isSoundOn = true;
    public GameObject pausePanel;
    public GameObject restartButton;
    public Text soundText;

    [Header("Score Stuff")]
    public Text scoreText;
    private float score = 0;
    public float Score
    {
        get { return score; }
    }

    [Header("Lose Panel")]
    public GameObject losePanel;
    public Text highScoreText;
    private int highScore = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        highScore = PlayerPrefs.GetInt("highScore", 0);
    }

    private void FixedUpdate()
    {
        score += 3 * Time.deltaTime;
        scoreText.text = "" + (int)score;
    }

    #region PauseCode 
    public void OnPause()
    {
        if (!restartButton.activeInHierarchy)
        {
            if (!isPaused)
            {
                Time.timeScale = 0;
                isPaused = true;
                pausePanel.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                isPaused = false;
                pausePanel.SetActive(false);
            }
        }
    }

    public void ActivatingSound() //Finish this part when music appeare
    {
        if (isSoundOn)
        {
            soundText.text = "Sound: ON";
            isSoundOn = false;
        }
        else
        {
            soundText.text = "Sound: OFF";
            isSoundOn = true;
        }
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    #endregion

    public void Restart()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
        losePanel.SetActive(false);
    }

    public void SetHighScore()
    {
        if (PlayerPrefs.HasKey("highScore"))
        {
            if ((int)score > highScore)
            {
                highScore = (int)score;
                PlayerPrefs.SetInt("highScore", highScore);
            }
        }
        else
        {
            highScore = (int)score;
            PlayerPrefs.SetInt("highScore", highScore);
        }

        highScoreText.text = highScore.ToString();
    }
}
